module.exports = {
  apps : [
    {
      name: "theinviteme-cms-dev",
      script: "npm",
      args: "run dev"
    },
    {
      name: "theinviteme-cms-prod",
      script: "npm",
      args: "run start"
    },
    {
      name: "theinviteme-cms-staging",
      script: "npm",
      args: "run start"
    }
  ]
}
