// import config from '~/store/config'
import {encryptAPI, decryptAPI} from '../plugins/global-function'
import {showNotification} from "../../theinviteme-cms/plugins/global-function";
export const state = () => ({
	listClient: [],
	paginate: {},
})
// ACTIONS AS METHODS
export const actions = { // asyncronous
	async nuxtServerInit({ commit }, { req }){
		// commit()
		// console.log('server init')
	},
	//region TEMPLATE
	async getListTemplate ({ commit }, payload) {
		// console.log(payload)
		await this.$axios.$get(`cms/customer/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				commit('setListClient', response)
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchTemplate ({ commit }, payload) {
		return await this.$axios.$get(`masterdata/service-center/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addTemplate ({ commit }, payload) {
		console.log(payload)
		return await this.$axios.post('cms/customer/add', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editTemplate ({ commit }, payload) {
		// console.log(payload)
		// showNotification('good', false, 'success')
		return await this.$axios.post('cms/whatsapp/template/update', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteTemplate({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/service-center/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteTemplate ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('masterdata/service-center/delete/'+payload)
			.then((response) => {
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion

	//region BLAST
	async sendBlast ({ commit }, payload) {
		await this.$axios.$get(`cms/whatsapp/send-blast-message/fid-event=${payload}`)
			.then((response) => {
				console.log(response)
				if (response.success){
					showNotification(response.message, false, 'success')
				}else{
					showNotification(response.message, false, 'danger')
				}
				// console.log(response)
				// commit('setListClient', response)
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListBlast ({ commit }, payload) {
		// console.log(payload)
		await this.$axios.$get(`cms/customer/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				commit('setListClient', response)
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchBlast ({ commit }, payload) {
		return await this.$axios.$get(`masterdata/service-center/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addBlast ({ commit }, payload) {
		return await this.$axios.post('cms/customer/add', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editBlast ({ commit }, payload) {
		// console.log(payload)
		// showNotification('good', false, 'success')
		return await this.$axios.post('cms/whatsapp/template/update', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteBlast({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/service-center/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteBlast ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('masterdata/service-center/delete/'+payload)
			.then((response) => {
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
}

// MUTATION AS LOGIC
export const mutations = { // syncronous
	setListClient (state, payload) {
		state.listClient = payload.data
		state.paginate = payload.paginate
	}
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
	listClients: state => {
		return state.listClient
	},

}
