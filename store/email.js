// import config from '~/store/config'
import {encryptAPI, decryptAPI, showNotification} from '../plugins/global-function'
export const state = () => ({
	tempImageFile: null,
	dataAddEmail: null,
	fileProgressPercentage: 0,
	statusEmail: 'add',
})
// ACTIONS AS METHODS
export const actions = { // asynchronous
	async nuxtServerInit({ commit }, { req }){
		// commit()
		console.log('server init')
	},
	//region RESEND EMAIL
	async resendEmailByEmailnId ({ commit }, payload) {
		return await this.$axios.$get(`cms/email/resend-email/id=${payload.id}/email=${payload.email}`)
			// return await this.$axios.$get(`cms/masterdata/content/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				if (response.success){
					showNotification(response.message, false, 'success')
				}else{
					showNotification(response.message, false, 'danger')
				}
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getListResendEmail ({ commit }, payload) {
		return await this.$axios.$get(`cms/email/resend-email/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			// return await this.$axios.$get(`cms/masterdata/content/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getListSearchResendEmail ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`cms/email/resend-email/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addResendEmail ({ commit }, payload) {
		// console.log(payload)
		// let formData = new FormData()
		// formData.append('name', payload.name)
		// formData.append('image_name', null)
		// formData.append('exclude_province_area', `{"name": [1, 2, 3, 4]}`)
		// formData.append('image', payload.images)
		// formData.append('Department_id', payload.Department_id)
		// formData.append('article_type_id', payload.article_type_id)
		// formData.append('title', payload.title)
		// formData.append('content', payload.content)
		// formData.append('publish_date', payload.publish_date)
		return await this.$axios.post('cms/Department/add', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editResendEmail ({ commit }, payload) {
		console.log(payload)
		// let formData = new FormData()
		// formData.append('id', payload.id)
		// formData.append('name', payload.name)
		return await this.$axios.post('cms/email/resend-email/update-email', payload,
		{
			// headers: {
			// 	'Content-Type': 'multipart/form-data'
			// },
			onUploadProgress: function( progressEvent ) {
				let uploadPercentage = parseInt( Math.round( ( progressEvent.loaded / progressEvent.total ) * 100 ));
				this.commit('setting/filePercentage', uploadPercentage)
			}.bind(this)
		})
	.then((response) => {
			if (response.data.success){
				showNotification(response.data.message, false, 'success')
			}else{
				showNotification(response.data.message, false, 'danger')
			}
			// console.log()
			return response.data
		})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
					return {
						success: false,
						message: err,
						info: 'some error'
					}
				}
			})
	},
	async editIsActiveResendEmail ({ commit }, payload) {
		// console.log(payload)
		let data = {
			"id": payload.id,
			"is_deleted": payload.is_deleted,
		}
		return await this.$axios.post('cms/Department/update-is-active', data)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteResendEmail ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/content/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteResendEmail ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('cms/department/delete/'+payload)
			.then((response) => {
				showNotification(response.data.message, false, 'success')
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion

	//region BLAST EMAIL
	async blastEmailByCycle ({ commit }, payload) {
		return await this.$axios.$get(`cms/email/blast-email/cycle=${payload}`)
			// return await this.$axios.$get(`cms/masterdata/content/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	//endregion

}

// MUTATION AS LOGIC
export const mutations = { // syncronous
	clearDataEmail(state){
		state.tempImageFile = null
		state.dataAddEmail = null
	},
	filePercentage(state, payload){
		state.fileProgressPercentage = payload
	},
	updateStatusEmail(state, payload){
		state.statusEmail = payload
	},
	savedStateAddEmail(state, payload){
		// console.log(payload)
		state.dataAddEmail = payload
	},
	setTempImageFile(state, payload){
		// state.tempImageFile = payload
		// state.dataAddDepartment.image_url = payload.name
		// console.log(state.tempImageFile)
	},
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
	listCategory: state => {
		return state.listCategory
	},

}


