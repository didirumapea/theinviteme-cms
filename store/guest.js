// import config from '~/store/config'
import {encryptAPI, decryptAPI} from '../plugins/global-function'
export const state = () => ({
	listClient: [],
	paginate: {},
})
// ACTIONS AS METHODS
export const actions = { // asyncronous
	async nuxtServerInit({ commit }, { req }){
		// commit()
		// console.log('server init')
	},
	//region GUEST
	async getListGuest ({ commit }, payload) {
		// console.log(payload)
		await this.$axios.$get(`cms/client/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				commit('setListClient', response)
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchGuest ({ commit }, payload) {
		return await this.$axios.$get(`masterdata/service-center/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addGuest ({ commit }, payload) {
		console.log(payload)
		let formData = new FormData()
		formData.append('fid_event', payload.fid_event)
		formData.append('file', payload.file)
		return await this.$axios.post('cms/guest/generate', formData,
			{
				headers: {
					'Content-Type': 'multipart/form-data',
					// 'Access-Control-Allow-Origin' : '*',
					// 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
				},
				onUploadProgress: function( progressEvent ) {
					let uploadPercentage = parseInt( Math.round( ( progressEvent.loaded / progressEvent.total ) * 100 ));
					commit('setting/filePercentage', uploadPercentage, { root: true })
				}.bind(this)
			})
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editGuest ({ commit }, payload) {
		// console.log(payload)
		// showNotification('good', false, 'success')
		let _data = {
			id: payload.id,
			name: payload.mName,
			address: payload.address,
			city: payload.city,
			phone: payload.phone,
			coordinat: {
				lat: payload.lat,
				lng: payload.lng
			}
		}
		return await this.$axios.post('masterdata/service-center/update', _data)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteGuest ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/service-center/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteGuest ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('masterdata/service-center/delete/'+payload)
			.then((response) => {
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
}

// MUTATION AS LOGIC
export const mutations = { // syncronous
	setListClient (state, payload) {
		state.listClient = payload.data
		state.paginate = payload.paginate
	}
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
	listClients: state => {
		return state.listClient
	},
}
